/*
 * battfink-apm.c
 * 
 * Copyright (C) 2003 Sun Microsystems, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Glynn Foster <glynn.foster@sun.com>
 */
#include <string.h>
#include <stdio.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_APM
#include <apm.h>
#endif

#include <libgnome/gnome-i18n.h>
#include <libgnomeui/gnome-stock-icons.h>
#include "battfink-apm.h"

/* We need to define some of these headers because seemingly
 * they aren't defined in every apm. Very mysterious
 */

#ifndef AC_LINE_STATUS_OFF
#define AC_LINE_STATUS_OFF      (0)
#endif

#ifndef AC_LINE_STATUS_ON
#define AC_LINE_STATUS_ON       (1)
#endif

#ifndef BATTERY_FLAGS_CHARGING
#define BATTERY_FLAGS_CHARGING  (0x8)
#endif

#ifndef BATTERY_FLAGS_ABSENT
#define BATTERY_FLAGS_ABSENT    (0x80)
#endif

#ifndef BATTERY_PERCENTAGE_UNKNOWN
#define BATTERY_PERCENTAGE_UNKNOWN  (-1)
#endif

#ifndef BATTERY_TIME_UNKNOWN
#define BATTERY_TIME_UNKNOWN        (-1)
#endif

/* These are the headers that are in apm.h - define above 
 * as we need them.
 *
 * #define AC_LINE_STATUS_OFF      (0)
 * #define AC_LINE_STATUS_ON       (1)
 * #define AC_LINE_STATUS_BACKUP   (2)
 * #define AC_LINE_STATUS_UNKNOWN  (0xff)
 * #define BATTERY_STATUS_HIGH     (0)
 * #define BATTERY_STATUS_LOW      (1)
 * #define BATTERY_STATUS_CRITICAL (2)
 * #define BATTERY_STATUS_ABSENT   (4) 
 * #define BATTERY_STATUS_UNKNOWN  (0xff) 
 * #define BATTERY_FLAGS_HIGH      (0x1)
 * #define BATTERY_FLAGS_LOW       (0x2)
 * #define BATTERY_FLAGS_CRITICAL  (0x4)
 * #define BATTERY_FLAGS_ABSENT    (0x80)
 */

typedef enum {
  BATTFINK_APM_HIGH,
  BATTFINK_APM_MEDIUM,
  BATTFINK_APM_LOW,
  BATTFINK_APM_CRITICAL,
  BATTFINK_APM_POWER,
  BATTFINK_APM_LAST
} BattfinkApmStatus;

struct _BattfinkApmPriv {
  GtkWidget  *image;
  GtkWidget  *hbox;
  GtkWidget  *label;
  GtkWidget  *time_menu_item;
  GtkWidget  *percent_menu_item;

  gboolean    ac_power;
  gboolean    battery_charging;
  guint       battery_status;
  guint       battery_percentage;
  guint       battery_flags;
  guint       battery_time;
  gboolean    battery_using_minutes;

  gint        timeout;
  gint        charging_timeout;

  GdkPixbuf      *battery_icon[BATTFINK_APM_LAST];
  GtkTooltips    *tooltips;
  GSList         *menu_group;
  gboolean        show_time;
};

enum {
  BATTFINK_SIG_PREFERENCE,
  BATTFINK_SIG_DISABLE,
  BATTFINK_SIG_DISABLE_LABEL,
  BATTFINK_SIG_LAST
};

#define BATTFINK_STOCK_HIGH      "battery-high"
#define BATTFINK_STOCK_NORM      "battery-norm"
#define BATTFINK_STOCK_LOW       "battery-low"
#define BATTFINK_STOCK_CRIT      "battery-crit"
#define BATTFINK_STOCK_AC        "battery-ac"

#define BATTFINK_DEFAULT_ICON_SIZE 16
static GtkIconSize battfink_icon_size = 0;
static gboolean battfink_icons_initialized = FALSE;

void battfink_apm_read_info (BattfinkApm *battfink_apm, GError **error);

static GObjectClass *parent_class;
static guint battfink_apm_signals [BATTFINK_SIG_LAST] = { 0 };

typedef enum {
  BATTFINK_APM_INSTALL_ERROR,
  BATTFINK_APM_VERSION_ERROR,
} BattfinkAPMError;

#define BATTFINK_APM_ERROR (battfink_apm_error_quark ())

GQuark
battfink_apm_error_quark (void)
{
  static GQuark quark = 0;

  if (quark == 0)
    quark = g_quark_from_static_string ("battfink-apm-error-quark");
  return quark;
}

void
battfink_apm_read_info (BattfinkApm *battfink_apm, GError **error) 
{
#ifdef HAVE_APM
  struct apm_info battery_info;
  int apm_ret;
  if (battfink_acpi_read_info (&battery_info))
    apm_ret = 0;
  else
    apm_ret = apm_read (&battery_info);

  switch (apm_ret) {
    case 0:
      /* We have a success. Start loading details into our struct */
      battfink_apm->priv->ac_power = battery_info.ac_line_status == AC_LINE_STATUS_ON ? TRUE : FALSE;
      battfink_apm->priv->battery_charging = (battery_info.battery_flags & BATTERY_FLAGS_CHARGING) ? TRUE : FALSE;
      battfink_apm->priv->battery_flags = battery_info.battery_flags;
      battfink_apm->priv->battery_status = battery_info.battery_status;
      battfink_apm->priv->battery_percentage = battery_info.battery_percentage;
      battfink_apm->priv->battery_time = battery_info.battery_time;
      battfink_apm->priv->battery_using_minutes = battery_info.using_minutes > 0 ? TRUE : FALSE;
      break;

    case 1:
      /* APM isn't installed */
      g_set_error (error, 
                   BATTFINK_APM_ERROR,
                   BATTFINK_APM_INSTALL_ERROR,
                   _("There is a problem with your APM installation"));
      break;

    case 2:
      /* APM is the wrong version */
      g_set_error (error,
                   BATTFINK_APM_ERROR,
                   BATTFINK_APM_VERSION_ERROR,
                   _("APM version is too old"));
      break;

    default:
      break;

  }
#endif

}

static gboolean 
battfink_apm_animate_charging (BattfinkApm *battfink_apm)
{
  static gint i = 3;

  if (i < 0)
    i = 3;
  gtk_image_set_from_pixbuf (GTK_IMAGE (battfink_apm->priv->image), battfink_apm->priv->battery_icon[i]);
  i--;
  return TRUE;
}

static gchar *
battfink_apm_get_time (BattfinkApm *battfink_apm)
{ 
  if (battfink_apm->priv->show_time && battfink_apm->priv->battery_time != BATTERY_TIME_UNKNOWN) {
    if (battfink_apm->priv->battery_time == 0)
      return g_strdup_printf ("");
    else {
      /* We have to pad the time with a zero */
      if ((battfink_apm->priv->battery_time % 60) < 10) {
        return g_strdup_printf (_("%d:0%d"),
                                battfink_apm->priv->battery_time / 60,
                                battfink_apm->priv->battery_time % 60);
      }
      else {
        return g_strdup_printf (_("%d:%d"),
                                battfink_apm->priv->battery_time / 60,
                                battfink_apm->priv->battery_time % 60);
      }
    }
  }
  else
    return g_strdup_printf (_("%d %%"),
                            battfink_apm->priv->battery_percentage);
}

static void
battfink_apm_update_tooltip (BattfinkApm *battfink_apm)
{
  gchar *tip;
  gchar *time;

  time = battfink_apm_get_time (battfink_apm);

  if (battfink_apm->priv->ac_power && battfink_apm->priv->battery_charging) 
    tip = g_strdup_printf (_("Charging: %s"), time);
  else if (battfink_apm->priv->ac_power && battfink_apm->priv->battery_charging)
    tip = g_strdup_printf (_("System is running on AC power"));
  else
    tip = g_strdup_printf (_("Battery: %s"), time);

  gtk_tooltips_set_tip (GTK_TOOLTIPS (battfink_apm->priv->tooltips), GTK_WIDGET (battfink_apm), tip, NULL);

  g_free (time);
  g_free (tip);
}


static void
battfink_apm_update_label (BattfinkApm *battfink_apm)
{
  gchar *label = NULL;

  label = battfink_apm_get_time (battfink_apm);
  gtk_label_set_text (GTK_LABEL (battfink_apm->priv->label), label);
}

static gboolean 
battfink_apm_update_image (BattfinkApm *battfink_apm)
{
  static gboolean first_time = FALSE;
  static gboolean changed_time = FALSE;
  GError *error = NULL;

  if (!battfink_apm->priv->ac_power && changed_time) {
    changed_time = FALSE;
    battfink_apm->priv->show_time = TRUE;
  }

  /* Convert to % while charging */
  if (battfink_apm->priv->ac_power) {
    if (battfink_apm->priv->show_time) {
      battfink_apm->priv->show_time = FALSE;
      changed_time = TRUE; 
    }
  }

  battfink_apm_read_info (battfink_apm, &error);

  if (error) {
    g_warning ("Failed to read APM information : %s", error->message);
    g_error_free (error);
  }

  if (battfink_apm->priv->battery_time != BATTERY_TIME_UNKNOWN || 
      battfink_apm->priv->battery_percentage != BATTERY_PERCENTAGE_UNKNOWN)
    battfink_apm_update_label (battfink_apm);

  battfink_apm_update_tooltip (battfink_apm);

  /* If the power is on and battery is not charging, then remove the timeout and show the 
   * power icon
   */
  if (battfink_apm->priv->ac_power && !battfink_apm->priv->battery_charging) {

    /* remove the timeout for animation if neccessary */
    if (battfink_apm->priv->charging_timeout != -1) {
        g_source_remove (battfink_apm->priv->charging_timeout);
        battfink_apm->priv->charging_timeout = -1;
    }

    gtk_image_set_from_pixbuf (GTK_IMAGE (battfink_apm->priv->image), battfink_apm->priv->battery_icon[BATTFINK_APM_POWER]);
    return TRUE;
  }

  /* If the power is on and the battery is charging, add the timeout
   */
  if (battfink_apm->priv->charging_timeout == -1 && 
      battfink_apm->priv->ac_power && 
      battfink_apm->priv->battery_charging) {

    if (!first_time) {
      /* This is a pretty ugly hack to get things to work properly */
      battfink_apm_animate_charging (battfink_apm);
      first_time = TRUE;
    }

    battfink_apm->priv->charging_timeout = g_timeout_add (3000, (GSourceFunc) battfink_apm_animate_charging, battfink_apm);
    return TRUE;
  }

  /* If the power is off, show the battery strength
   */
  if (!battfink_apm->priv->ac_power) {

    if (battfink_apm->priv->charging_timeout != -1) {
        g_source_remove (battfink_apm->priv->charging_timeout);
        battfink_apm->priv->charging_timeout = -1;
    }

    if (battfink_apm->priv->battery_percentage > 75 && battfink_apm->priv->battery_percentage <= 100) {
      gtk_image_set_from_pixbuf (GTK_IMAGE (battfink_apm->priv->image), 
                                 battfink_apm->priv->battery_icon[BATTFINK_APM_HIGH]);
    }
    else if (battfink_apm->priv->battery_percentage > 50 && battfink_apm->priv->battery_percentage <= 75) {
      gtk_image_set_from_pixbuf (GTK_IMAGE (battfink_apm->priv->image), 
                                 battfink_apm->priv->battery_icon[BATTFINK_APM_MEDIUM]);
    }
    else if (battfink_apm->priv->battery_percentage > 25 && battfink_apm->priv->battery_percentage <= 50) {
      gtk_image_set_from_pixbuf (GTK_IMAGE (battfink_apm->priv->image), 
                                 battfink_apm->priv->battery_icon[BATTFINK_APM_LOW]);
    }
    else {
      gtk_image_set_from_pixbuf (GTK_IMAGE (battfink_apm->priv->image), 
                                 battfink_apm->priv->battery_icon[BATTFINK_APM_CRITICAL]);
    }

    return TRUE;
  }

  return TRUE;
}

static void
battfink_apm_enable (BattfinkApm *battfink_apm, EggTrayIcon *tray_icon)
{
  
  if (battfink_apm->priv->battery_flags & BATTERY_FLAGS_ABSENT) {
    gtk_image_set_from_pixbuf (GTK_IMAGE (battfink_apm->priv->image), battfink_apm->priv->battery_icon[BATTFINK_APM_POWER]);
    gtk_tooltips_set_tip (GTK_TOOLTIPS (battfink_apm->priv->tooltips), GTK_WIDGET (battfink_apm), 
                          _("System is running on AC power"), NULL);
    
    gtk_widget_show_all (GTK_WIDGET (tray_icon));
    gtk_widget_hide (battfink_apm->priv->label);
    return;
  }

  battfink_apm_update_image (battfink_apm);

  gtk_widget_show_all (GTK_WIDGET (tray_icon));
  gtk_widget_hide (battfink_apm->priv->label);

  if (battfink_apm->priv->timeout == -1)
    battfink_apm->priv->timeout = g_timeout_add (3000, (GSourceFunc) battfink_apm_update_image, battfink_apm);
}

static void
battfink_apm_enable_label (BattfinkApm *battfink_apm, EggTrayIcon *tray_icon)
{
  
  if (battfink_apm->priv->battery_flags & BATTERY_FLAGS_ABSENT) {
    gtk_image_set_from_pixbuf (GTK_IMAGE (battfink_apm->priv->image), battfink_apm->priv->battery_icon[BATTFINK_APM_POWER]);
    gtk_tooltips_set_tip (GTK_TOOLTIPS (battfink_apm->priv->tooltips), GTK_WIDGET (battfink_apm), 
                          _("System is running on AC power"), NULL);
    
    gtk_widget_show_all (GTK_WIDGET (tray_icon));
    return;
  }

  battfink_apm_update_label (battfink_apm);

  gtk_widget_show_all (GTK_WIDGET (tray_icon));

  if (battfink_apm->priv->timeout == -1)
    battfink_apm->priv->timeout = g_timeout_add (3000, (GSourceFunc) battfink_apm_update_image, battfink_apm);
}

static void
battfink_apm_disable (BattfinkApm *battfink_apm, EggTrayIcon *tray_icon)
{
  gtk_widget_hide (GTK_WIDGET (tray_icon));

  g_source_remove (battfink_apm->priv->timeout);

  if (battfink_apm->priv->charging_timeout != -1)
    g_source_remove (battfink_apm->priv->charging_timeout);

  battfink_apm->priv->charging_timeout = -1;
  battfink_apm->priv->timeout = -1;
}

static void
battfink_apm_disable_label (BattfinkApm *battfink_apm, EggTrayIcon *tray_icon)
{
  gtk_widget_hide (battfink_apm->priv->label);
}

static gint
battfink_apm_time_left (BattfinkApm *battfink_apm)
{
  GError *error = NULL;

  battfink_apm_read_info (battfink_apm, &error);

  if (error) {
    g_warning ("Failed to read APM information : %s", error->message);
    g_error_free (error);
    return -1;
  }
  
  if (battfink_apm->priv->ac_power)
    return -1;
  else
    return battfink_apm->priv->battery_time;
}

typedef struct
{
  char *stock_id;
  const guint8 *icon_data;
} BattfinkStockIcon;

static BattfinkStockIcon items[] = {
  { BATTFINK_STOCK_HIGH, BATTFINK_ICONDIR"/battfink-100.png" },
  { BATTFINK_STOCK_NORM, BATTFINK_ICONDIR"/battfink-75.png" },
  { BATTFINK_STOCK_LOW, BATTFINK_ICONDIR"/battfink-50.png" },
  { BATTFINK_STOCK_CRIT,BATTFINK_ICONDIR"/battfink-25.png" },
  { BATTFINK_STOCK_AC,  BATTFINK_ICONDIR"/battfink-charge.png" }
};

static void
battfink_apm_register_stock_icons (GtkIconFactory *factory)
{
  GtkIconSource *source;
  int i;
  
  source = gtk_icon_source_new ();
  for (i = 0; i < G_N_ELEMENTS (items); i++) {
    GtkIconSet *icon_set;
  
    gtk_icon_source_set_filename (source, items[i].icon_data);
    icon_set = gtk_icon_set_new ();
    gtk_icon_set_add_source (icon_set, source);
    gtk_icon_factory_add (factory, items[i].stock_id, icon_set);
    gtk_icon_set_unref (icon_set);
  }
  gtk_icon_source_free (source);
}

static void
battfink_apm_init_stock_icons ()
{
  GtkIconFactory *factory;

  if (battfink_icons_initialized)
    return;

  battfink_icon_size = gtk_icon_size_register ("panel-menu", 
                                               BATTFINK_DEFAULT_ICON_SIZE, 
                                               BATTFINK_DEFAULT_ICON_SIZE);
  factory = gtk_icon_factory_new ();
  gtk_icon_factory_add_default (factory);
  battfink_apm_register_stock_icons (factory);
  g_object_unref (factory);
  battfink_icons_initialized = TRUE;
}

static void
battfink_apm_init_icons (BattfinkApm *battfink_apm)
{
  if (battfink_apm->priv->battery_icon[BATTFINK_APM_HIGH])
    g_object_unref (battfink_apm->priv->battery_icon[BATTFINK_APM_HIGH]);
  if (battfink_apm->priv->battery_icon[BATTFINK_APM_MEDIUM])
    g_object_unref (battfink_apm->priv->battery_icon[BATTFINK_APM_MEDIUM]);
  if (battfink_apm->priv->battery_icon[BATTFINK_APM_LOW])
    g_object_unref (battfink_apm->priv->battery_icon[BATTFINK_APM_LOW]);
  if (battfink_apm->priv->battery_icon[BATTFINK_APM_CRITICAL])
    g_object_unref (battfink_apm->priv->battery_icon[BATTFINK_APM_CRITICAL]);
  if (battfink_apm->priv->battery_icon[BATTFINK_APM_POWER])
    g_object_unref (battfink_apm->priv->battery_icon[BATTFINK_APM_POWER]);

  battfink_apm->priv->battery_icon[BATTFINK_APM_HIGH] = gtk_widget_render_icon (GTK_WIDGET (battfink_apm),
                                                                                BATTFINK_STOCK_HIGH,
                                                                                battfink_icon_size, NULL);

  battfink_apm->priv->battery_icon[BATTFINK_APM_MEDIUM] = gtk_widget_render_icon (GTK_WIDGET (battfink_apm),
                                                                                  BATTFINK_STOCK_NORM,
                                                                                  battfink_icon_size, NULL);

  battfink_apm->priv->battery_icon[BATTFINK_APM_LOW] = gtk_widget_render_icon (GTK_WIDGET (battfink_apm),
                                                                               BATTFINK_STOCK_LOW,
                                                                               battfink_icon_size, NULL);

  battfink_apm->priv->battery_icon[BATTFINK_APM_CRITICAL] = gtk_widget_render_icon (GTK_WIDGET (battfink_apm),
                                                                                    BATTFINK_STOCK_CRIT,
                                                                                    battfink_icon_size, NULL);

  battfink_apm->priv->battery_icon[BATTFINK_APM_POWER] = gtk_widget_render_icon (GTK_WIDGET (battfink_apm),
                                                                                 BATTFINK_STOCK_AC,
                                                                                 battfink_icon_size, NULL);
}

static void
battfink_apm_position_menu (GtkMenu *menu, int *x, int *y, gboolean *push_in, gpointer user_data)
{
  GtkWidget *widget = GTK_WIDGET (user_data);
  GtkRequisition requisition;
  int menu_xpos;
  int menu_ypos;

  gtk_widget_size_request (GTK_WIDGET (menu), &requisition);
  gdk_window_get_origin (widget->window, &menu_xpos, &menu_ypos);

  menu_xpos += widget->allocation.x;
  menu_ypos += widget->allocation.y;

  if (menu_ypos > gdk_screen_get_height (gtk_widget_get_screen (widget)) / 2)
    menu_ypos -= requisition.height;
  else
    menu_ypos += widget->allocation.height;

  *x = menu_xpos;
  *y = menu_ypos;
  *push_in = TRUE;

}

static void
battfink_apm_change_label_callback (GtkWidget *widget, gpointer user_data)
{
  BattfinkApm *battfink_apm;
  gboolean show_time;

  battfink_apm = BATTFINK_APM (user_data);
  show_time = gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (battfink_apm->priv->time_menu_item));

  if (show_time)
    battfink_apm->priv->show_time = TRUE;
  else
    battfink_apm->priv->show_time = FALSE;

  battfink_apm_update_label (battfink_apm);

}

static void
battfink_apm_remove_icon_callback (GtkWidget *widget, gpointer user_data)
{
  g_signal_emit (G_OBJECT (user_data), battfink_apm_signals [BATTFINK_SIG_DISABLE], 0);
}

static void
battfink_apm_remove_label_callback (GtkWidget *widget, gpointer user_data)
{
  g_signal_emit (G_OBJECT (user_data), battfink_apm_signals [BATTFINK_SIG_DISABLE_LABEL], 0);
}

static void
battfink_apm_open_preferences (GtkWidget *widget, gpointer user_data)
{
  g_signal_emit (G_OBJECT (user_data), battfink_apm_signals [BATTFINK_SIG_PREFERENCE], 0);
}

static void
battfink_apm_popup_menu (BattfinkApm *battfink_apm, guint button, guint32 activate_time)
{
  GtkWidget *menu;
  GtkWidget *menu_item;
  GtkWidget *image;
  GError *error = NULL;

  battfink_apm_read_info (battfink_apm, &error);

  if (error) {
    g_warning ("Failed to read APM information : %s", error->message);
    g_error_free (error);
    return;
  }

  menu = gtk_menu_new ();

  menu_item = gtk_menu_item_new_with_mnemonic (_("_Open Energy Saving Preferences"));
  g_signal_connect (G_OBJECT (menu_item), "activate",
                    G_CALLBACK (battfink_apm_open_preferences), battfink_apm);
  gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

  menu_item = gtk_separator_menu_item_new ();
  gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

  if (battfink_apm->priv->battery_time > 0
      && !(battfink_apm->priv->battery_flags & BATTERY_FLAGS_ABSENT)
      && !battfink_apm->priv->battery_charging) {
    battfink_apm->priv->time_menu_item = gtk_radio_menu_item_new_with_mnemonic (battfink_apm->priv->menu_group, 
                                                                                _("_Time Remaining"));
    battfink_apm->priv->menu_group = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (battfink_apm->priv->time_menu_item));
    g_signal_connect (G_OBJECT (battfink_apm->priv->time_menu_item), "toggled", 
                      G_CALLBACK (battfink_apm_change_label_callback), battfink_apm);

    if (battfink_apm->priv->show_time)
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (battfink_apm->priv->time_menu_item), TRUE);

    gtk_menu_shell_append (GTK_MENU_SHELL (menu), battfink_apm->priv->time_menu_item);

    battfink_apm->priv->percent_menu_item = gtk_radio_menu_item_new_with_mnemonic (battfink_apm->priv->menu_group, 
                                                                                   _("_Percentage"));

    g_signal_connect (G_OBJECT (battfink_apm->priv->percent_menu_item), "toggled", 
                      G_CALLBACK (battfink_apm_change_label_callback), battfink_apm);

    if (!battfink_apm->priv->show_time)
      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (battfink_apm->priv->percent_menu_item), TRUE);

    gtk_menu_shell_append (GTK_MENU_SHELL (menu), battfink_apm->priv->percent_menu_item);

    menu_item = gtk_separator_menu_item_new ();
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);
  }

  menu_item = gtk_image_menu_item_new_with_mnemonic (_("_Remove Icon"));
  image = gtk_image_new_from_stock (GTK_STOCK_QUIT, GTK_ICON_SIZE_MENU);
  gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (menu_item), image);
  g_signal_connect (G_OBJECT (menu_item), "activate",
                    G_CALLBACK (battfink_apm_remove_icon_callback), battfink_apm);
  gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

  gtk_widget_show_all (menu);

  gtk_menu_popup (GTK_MENU (menu), NULL, NULL, battfink_apm_position_menu,
                  GTK_WIDGET (battfink_apm), button, activate_time);
}

static gboolean
battfink_apm_button_press_event (GtkWidget *widget, GdkEventButton *event)
{
  if (event->button == 1) {
    g_signal_emit (G_OBJECT (widget), battfink_apm_signals [BATTFINK_SIG_PREFERENCE], 0);
    return TRUE;
  }

  if (event->button == 3) {
    battfink_apm_popup_menu (BATTFINK_APM (widget), event->button, event->time);
    return TRUE;
  }

  return FALSE;
}

static void
battfink_apm_finalize (GObject *object)
{
  BattfinkApm *battfink_apm = BATTFINK_APM (object);
  gint i;

  for (i = 0; i < BATTFINK_APM_LAST; i++) {
    if (battfink_apm->priv->battery_icon[i]) 
      g_object_unref (battfink_apm->priv->battery_icon[i]);
    battfink_apm->priv->battery_icon[i] = NULL;
  }

  /* Remove the timeouts */
  if (battfink_apm->priv->timeout != -1) {
    g_source_remove (battfink_apm->priv->timeout);
    battfink_apm->priv->timeout = -1;
  }
  if (battfink_apm->priv->charging_timeout != -1) {
    g_source_remove (battfink_apm->priv->charging_timeout);
    battfink_apm->priv->charging_timeout = -1;
  }

  g_free (battfink_apm->priv);
  battfink_apm->priv = NULL;

  parent_class->finalize (object);
}

static void
battfink_apm_destroy (GtkObject *object)
{
  BattfinkApm *battfink_apm = BATTFINK_APM (object);

  if (battfink_apm->priv->tooltips)
    g_object_unref (battfink_apm->priv->tooltips);
  battfink_apm->priv->tooltips = NULL;

  battfink_apm->priv->image = NULL;

  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
battfink_apm_class_init (BattfinkApmClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GtkObjectClass *gtkobject_class = GTK_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
 
  parent_class = g_type_class_peek_parent (klass);

  gobject_class->finalize = battfink_apm_finalize;
  gtkobject_class->destroy = battfink_apm_destroy;

  klass->enable  = battfink_apm_enable;
  klass->disable = battfink_apm_disable;
  klass->enable_label = battfink_apm_enable_label;
  klass->disable_label = battfink_apm_disable_label;
  klass->time_left = battfink_apm_time_left;

  widget_class->button_press_event = battfink_apm_button_press_event;

  battfink_apm_signals [BATTFINK_SIG_PREFERENCE] = g_signal_new ("preferences", 
                                                                  G_OBJECT_CLASS_TYPE (gobject_class), 
                                                                  G_SIGNAL_RUN_FIRST,
                                                                  G_STRUCT_OFFSET (BattfinkApmClass, preferences),
                                                                  NULL, NULL,
                                                                  g_cclosure_marshal_VOID__VOID,
                                                                  G_TYPE_NONE, 0);

  battfink_apm_signals [BATTFINK_SIG_DISABLE] = g_signal_new ("disable", 
                                                               G_OBJECT_CLASS_TYPE (gobject_class), 
                                                               G_SIGNAL_RUN_FIRST, 
                                                               G_STRUCT_OFFSET (BattfinkApmClass, icon_disable),
                                                               NULL, NULL,
                                                               g_cclosure_marshal_VOID__VOID,
                                                               G_TYPE_NONE, 0);

  battfink_apm_signals [BATTFINK_SIG_DISABLE] = g_signal_new ("disable_label", 
                                                               G_OBJECT_CLASS_TYPE (gobject_class), 
                                                               G_SIGNAL_RUN_FIRST, 
                                                               G_STRUCT_OFFSET (BattfinkApmClass, label_disable),
                                                               NULL, NULL,
                                                               g_cclosure_marshal_VOID__VOID,
                                                               G_TYPE_NONE, 0);
}

static void
battfink_apm_style_set (GtkWidget *widget, GtkStyle *prev_style, gpointer user_data)
{
  BattfinkApm *battfink_apm;

  battfink_apm = BATTFINK_APM (widget);

  battfink_apm_init_icons (battfink_apm);
}

static void
battfink_apm_init (BattfinkApm *battfink_apm,
                   BattfinkApmClass *klass)
{
  GError *error = NULL;

  battfink_apm->priv = g_new0 (BattfinkApmPriv, 1);
  battfink_apm->priv->image = gtk_image_new ();
  battfink_apm->priv->timeout = -1;
  battfink_apm->priv->charging_timeout = -1;

  battfink_apm->priv->label = gtk_label_new ("");
  battfink_apm->priv->hbox = gtk_hbox_new (FALSE, 0);

  gtk_box_pack_start (GTK_BOX (battfink_apm->priv->hbox), battfink_apm->priv->image, FALSE, FALSE, 1);
  gtk_box_pack_end (GTK_BOX (battfink_apm->priv->hbox), battfink_apm->priv->label, FALSE, FALSE, 1);
  gtk_container_add (GTK_CONTAINER (battfink_apm), battfink_apm->priv->hbox);

  battfink_apm->priv->tooltips = gtk_tooltips_new ();
  g_object_ref (G_OBJECT (battfink_apm->priv->tooltips));
  gtk_object_sink (GTK_OBJECT (battfink_apm->priv->tooltips));

  gtk_widget_add_events (GTK_WIDGET (battfink_apm), 
                         GDK_BUTTON_PRESS_MASK | GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK);

  g_signal_connect (battfink_apm, "style-set",
                    G_CALLBACK (battfink_apm_style_set), NULL);

  battfink_apm_init_stock_icons ();
  battfink_apm_init_icons (battfink_apm);

  battfink_apm_read_info (battfink_apm, &error);

  if (error) {
    g_warning ("Failed to read APM information : %s", error->message);
    g_error_free (error);
  }

}


GType
battfink_apm_get_type (void)
{
  static GType type = 0;

  if (!type) {
    static const GTypeInfo info = {
      sizeof (BattfinkApmClass),
      NULL,
      NULL,
      (GClassInitFunc) battfink_apm_class_init,
      NULL,
      NULL,
      sizeof (BattfinkApm),
      0,
      (GInstanceInitFunc) battfink_apm_init,
      NULL
    };

    type = g_type_register_static (GTK_TYPE_EVENT_BOX, "BattfinkApm", &info, 0);
  }

  return type;
}

GtkWidget *
battfink_apm_new (void)
{
  return g_object_new (BATTFINK_TYPE_APM, NULL);
}
