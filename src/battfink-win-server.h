#ifndef __BATTFINK_WINDOW_SERVER_H
#define __BATTFINK_WINDOW_SERVER_H

#include <bonobo/bonobo-control.h>
#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-window.h>
#include "GNOME_Battfink.h"

G_BEGIN_DECLS

#define BATTFINK_WINDOW_SERVER_TYPE         (battfink_window_server_get_type ())
#define BATTFINK_WINDOW_SERVER(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), BATTFINK_WINDOW_SERVER_TYPE, BattfinkWindowServer))
#define BATTFINK_WINDOW_SERVER_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), BATTFINK_WINDOW_SERVER_TYPE, BattfinkWindowServerClass))
#define BATTFINK_WINDOW_SERVER_IS_OBJECT(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), BATTFINK_WINDOW_SERVER_TYPE))
#define BATTFINK_WINDOW_SERVER_IS_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), BATTFINK_WINDOW_SERVER_TYPE))
#define BATTFINK_WINDOW_SERVER_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), BATTFINK_WINDOW_SERVER_TYPE, BattfinkWindowServerClass))

typedef struct
{
        BonoboObject parent;

	GtkWindow *win;
} BattfinkWindowServer;

typedef struct
{
        BonoboObjectClass parent_class;

        POA_GNOME_Battfink_Window__epv epv;
} BattfinkWindowServerClass;

GType          battfink_window_server_get_type (void);

BonoboObject  *battfink_window_server_new      (GtkWindow *win);

G_END_DECLS

#endif /* __BATTFINK_WINDOW_SERVER_H */
