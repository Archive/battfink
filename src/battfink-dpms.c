/*
* battfink-dpms.c
*
* Copyright (C) 2003 Sun Microsystems, Inc.
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the
* Free Software Foundation, Inc., 59 Temple Place - Suite 330,
* Boston, MA 02111-1307, USA.
*
* Authors: Glynn Foster <glynn.foster@sun.com>
*/

#include <stdio.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gdk/gdkx.h>
#include <libgnome/gnome-i18n.h>

#include "battfink-dpms.h"

#ifdef HAVE_EXTENSION_DPMS

#include <X11/Xlib.h>
#include <X11/Xproto.h>
#include <X11/extensions/dpms.h>
#include <X11/extensions/dpmsstr.h>

extern Bool   DPMSQueryExtension  (Display *dpy, int *event_ret, int *err_ret);
extern Bool   DPMSCapable         (Display *dpy);
extern Status DPMSInfo            (Display *dpy, CARD16 *power_level, BOOL *state);
extern Status DPMSSetTimeouts     (Display *dpy, CARD16 standby, CARD16 suspend, CARD16 off);
extern Bool   DPMSGetTimeouts     (Display *dpy, CARD16 *standby, CARD16 *suspend, CARD16 *off);
extern Status DPMSEnable          (Display *dpy);
extern Status DPMSDisable         (Display *dpy);


#endif /* HAVE_EXTENSION_DPMS */

GQuark
battfink_dpms_error_quark (void)
{
  static GQuark quark = 0;

  if (quark == 0)
      quark = g_quark_from_static_string ("battfink-dpms-error-quark");
  return quark;
}

void
battfink_set_dpms_timeouts (gint standby, gint suspend, gint shutdown, GError **error)
{
#ifdef HAVE_EXTENSION_DPMS
  CARD16 standby_val, suspend_val, shutdown_val;
  int query_event, query_error;

  if (!DPMSQueryExtension (GDK_DISPLAY(), &query_event, &query_error)) {
    /* We don't support DPMS extension */
    g_set_error (error,
                 BATTFINK_DPMS_ERROR,
                 BATTFINK_DPMS_QUERY_ERROR,
                 _("X server does not support DPMS extension"));
    return;
  }

  if (!DPMSCapable (GDK_DISPLAY())) {
    /* DPMS sin't supported on this monitor */
    g_set_error (error,
                 BATTFINK_DPMS_ERROR,
                 BATTFINK_DPMS_SUPPORT_ERROR,
                 _("Display is not DPMS compliant"));
    return;
  }

  if (!DPMSGetTimeouts (GDK_DISPLAY(), &standby_val, &suspend_val, &shutdown_val)) {
    /* Can't get existing DPMS timeouts */
    g_set_error (error,
                 BATTFINK_DPMS_ERROR,
                 BATTFINK_DPMS_GET_ERROR,
                 _("Failed to get display power management timeouts"));
    return;
  }

  if (standby != standby_val || suspend != suspend_val || shutdown != shutdown_val) {
    /* We have different values - set DPMS */
    if (!DPMSSetTimeouts (GDK_DISPLAY(), standby, suspend, shutdown)) {
      /* Can't get DPMS timeouts */
    g_set_error (error,
                 BATTFINK_DPMS_ERROR,
                 BATTFINK_DPMS_SET_ERROR,
                 _("Failed to set display power management timeouts"));
      return;
    }
  } 
#endif /* HAVE_EXTENSION_DPMS */
  return;
}

void
battfink_get_dpms_timeouts (gint *standby, gint *suspend, gint *shutdown, GError **error)
{
#ifdef HAVE_EXTENSION_DPMS
  BOOL power_state;
  CARD16 power_level;
  CARD16 standby_val, suspend_val, shutdown_val;
  int query_event, query_error;

  if (!DPMSQueryExtension (GDK_DISPLAY(), &query_event, &query_error)) {
    /* We don't support DPMS extension */
    g_set_error (error,
                 BATTFINK_DPMS_ERROR,
                 BATTFINK_DPMS_QUERY_ERROR,
                 _("X server does not support DPMS extension"));
    return;
  }

  if (!DPMSCapable (GDK_DISPLAY())) {
    /* DPMS isn't supported on this monitor */
    g_set_error (error,
                 BATTFINK_DPMS_ERROR,
                 BATTFINK_DPMS_SUPPORT_ERROR,
                 _("Display is not DPMS compliant"));
    return;
  }

  if (!DPMSGetTimeouts (GDK_DISPLAY(), &standby_val, &suspend_val, &shutdown_val)) {
    /* Can't get DPMS timeouts */
    g_set_error (error,
                 BATTFINK_DPMS_ERROR,
                 BATTFINK_DPMS_GET_ERROR,
                 _("Failed to get display power management timeouts"));
    return;
  }
  
  *standby = standby_val;
  *suspend = suspend_val;
  *shutdown = shutdown_val;

  return;

#endif /* HAVE_EXTENSION_DPMS */
  return;
}

gboolean
battfink_check_dpms_enabled (GError **error)
{
#ifdef HAVE_EXTENSION_DPMS
  BOOL power_state;
  CARD16 power_level;
  int query_event, query_error;

  if (!DPMSQueryExtension (GDK_DISPLAY(), &query_event, &query_error)) {
    /* We don't support DPMS extension */
    g_set_error (error,
                 BATTFINK_DPMS_ERROR,
                 BATTFINK_DPMS_QUERY_ERROR,
                 _("X server does not support DPMS extension"));
    return FALSE;
  }

  if (!DPMSCapable (GDK_DISPLAY())) {
    /* DPMS isn't supported on this monitor */
    g_set_error (error,
                 BATTFINK_DPMS_ERROR,
                 BATTFINK_DPMS_SUPPORT_ERROR,
                 _("Display is not DPMS compliant"));
    return FALSE;
  }

  if (!DPMSInfo (GDK_DISPLAY(), &power_level, &power_state)) {
    /* Can't get DPMS info */
    g_set_error (error,
                 BATTFINK_DPMS_ERROR,
                 BATTFINK_DPMS_INFO_ERROR,
                 _("Failed to retrieve DPMS information"));
    return FALSE;
  }

  return power_state;

#endif /* HAVE_EXTENSION_DPMS */
  return FALSE;
}

void
battfink_dpms_enable (gboolean state, GError **error)
{
#ifdef HAVE_EXTENSION_DPMS
  if (state) {
    if (!DPMSEnable (GDK_DISPLAY())) {
      g_set_error (error,
                   BATTFINK_DPMS_ERROR,
                   BATTFINK_DPMS_ENABLE_ERROR,
                   _("Failed to enable DPMS"));
    }
  } else {
      if (!DPMSDisable (GDK_DISPLAY())) {
      g_set_error (error,
                   BATTFINK_DPMS_ERROR,
                   BATTFINK_DPMS_DISABLE_ERROR,
                   _("Failed to disable DPMS"));
      }
  }
#endif /* HAVE_EXTENSION_DPMS */
}
