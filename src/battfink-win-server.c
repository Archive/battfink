/*
 * battfink-win-server.c
 *
 * Copyright (C) 2003 Sun Microsystems, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Glynn Foster <glynn.foster@sun.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <bonobo/bonobo-generic-factory.h>
#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-context.h>

#include "battfink-win-server.h"
#include "GNOME_Battfink.h"
#include "header.h"

static void battfink_window_server_class_init (BattfinkWindowServerClass *klass);
static void battfink_window_server_init (BattfinkWindowServer *a);
static void battfink_window_server_object_finalize (GObject *object);
static GObjectClass *battfink_window_server_parent_class;

BonoboObject *
battfink_window_server_new (GtkWindow *win)
{
	BattfinkWindowServer *server;

	server = g_object_new (BATTFINK_WINDOW_SERVER_TYPE, NULL);
	server->win = win;
	return BONOBO_OBJECT (server);
}

static void
impl_battfink_window_server_grabFocus (PortableServer_Servant _servant,
				       CORBA_Environment * ev)
{
        GtkWindow *win;
        
        win = battfink_client_get_active_window (fink_client);
        gtk_window_present (GTK_WINDOW (win));
}

static void
battfink_window_server_class_init (BattfinkWindowServerClass *klass)
{
        GObjectClass *object_class = (GObjectClass *) klass;
        POA_GNOME_Battfink_Window__epv *epv = &klass->epv;

        battfink_window_server_parent_class = g_type_class_peek_parent (klass);

        object_class->finalize = battfink_window_server_object_finalize;

        /* connect implementation callbacks */
	epv->grabFocus   = impl_battfink_window_server_grabFocus;
}

static void
battfink_window_server_init (BattfinkWindowServer *c) 
{
}

static void
battfink_window_server_object_finalize (GObject *object)
{
        BattfinkWindowServer *a = BATTFINK_WINDOW_SERVER (object);

        battfink_window_server_parent_class->finalize (G_OBJECT (a));
}

BONOBO_TYPE_FUNC_FULL (
        BattfinkWindowServer,                    
        GNOME_Battfink_Window, 
        BONOBO_TYPE_OBJECT,           
        battfink_window_server);
