#ifndef __BATTFINK_APM_H__
#define __BATTFINK_APM_H__

#include "config.h"
#include <gtk/gtk.h>
#include "eggtrayicon.h"
#include "battfink-client.h"

#define BATTFINK_TYPE_APM         (battfink_apm_get_type ())
#define BATTFINK_APM(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), BATTFINK_TYPE_APM, BattfinkApm))
#define BATTFINK_APM_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), BATTFINK_TYPE_APM, BattfinkApmClass))
#define BATTFINK_IS_APM(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), BATTFINK_TYPE_APM))
#define BATTFINK_IS_APM_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), BATTFINK_TYPE_APM))
#define BATTFINK_APM_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), BATTFINK_TYPE_APM, BattfinkApmClass))

typedef struct _BattfinkApm         BattfinkApm;
typedef struct _BattfinkApmClass    BattfinkApmClass;
typedef struct _BattfinkApmPriv     BattfinkApmPriv;

struct _BattfinkApm {
  GtkEventBox      window_instance;

  BattfinkApmPriv *priv;
  BattfinkClient *parent;
};

struct _BattfinkApmClass {
  GtkWindowClass window_class;

  void    (*enable)        (BattfinkApm *battfink_apm, EggTrayIcon *tray_icon);
  void    (*disable)       (BattfinkApm *battfink_apm, EggTrayIcon *tray_icon);
  void    (*enable_label)  (BattfinkApm *battfink_apm, EggTrayIcon *tray_icon);
  void    (*disable_label) (BattfinkApm *battfink_apm, EggTrayIcon *tray_icon);
  gint    (*time_left)     (BattfinkApm *battfink_apm);

  void    (*preferences)   (BattfinkApm *battfink_apm);
  void    (*icon_disable)  (BattfinkApm *battfink_apm);
  void    (*label_disable) (BattfinkApm *battfink_apm);
};

GType       battfink_apm_get_type (void) G_GNUC_CONST;
GtkWidget*  battfink_apm_new (void);

#endif /* __BATTFINK_APM_H__ */
