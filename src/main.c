/*
 * main.c
 *
 * Copyright (C) 2003 Sun Microsystems, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Glynn Foster <glynn.foster@sun.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <bonobo/bonobo-object.h>
#include <gnome.h>

#include "battfink-app-server.h"
#include "battfink-apm.h"
#include "header.h"

#undef BATTFINK_DEBUG

BattfinkClient *fink_client = NULL;

BonoboObject *battfink_app_server;

gboolean new_window_option = FALSE;
gboolean quit_option = FALSE;

static const struct poptOption options [] = {
  { "new-window", '\0', POPT_ARG_NONE, &new_window_option, 0, N_("Create a new window"), NULL },
  { "quit", '\0', POPT_ARG_NONE, &quit_option, 0, N_("Quit application"), NULL },
  { NULL, '\0', 0, NULL, 0}
};

static void
battfink_parse_commandline (void)
{
  CORBA_Environment env;
  GNOME_Battfink_Application server;
  GNOME_Battfink_Window window;

  CORBA_exception_init (&env);
  server = bonobo_activation_activate_from_id ("OAFIID:GNOME_Battfink_Application", 0, NULL, &env);

  g_return_if_fail (server != NULL);

  if (quit_option) {
    GNOME_Battfink_Application_quit (server, &env);
  }
  if (new_window_option) {
    GNOME_Battfink_Application_newWindow (server, &env);
  }

  if (!quit_option) {
    window = GNOME_Battfink_Application_getActiveWindow (server, &env);
    GNOME_Battfink_Window_grabFocus (window, &env);
  }

  bonobo_object_release_unref (server, &env);
  CORBA_exception_free (&env);
}

int main (int argc, char *argv[])
{
  GnomeClient *client;
  CORBA_Object factory;
  gboolean restored = FALSE;

  bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  gnome_program_init ("battfink", VERSION,
                      LIBGNOMEUI_MODULE, argc, argv,
                      GNOME_PARAM_POPT_TABLE, options,
                      NULL);

  /* check whether we are running already */
  
  factory = bonobo_activation_activate_from_id ("OAFIID:GNOME_Battfink_Factory",
                                                Bonobo_ACTIVATION_FLAG_EXISTING_ONLY, NULL, NULL);

  if (factory != NULL) {
    battfink_parse_commandline ();
    exit (0);
  }

  /* Let's create a new client */
  client = gnome_master_client ();
  gnome_client_set_restart_style (client, GNOME_RESTART_ANYWAY);
  gnome_client_flush (client);

  fink_client = battfink_client_new ();

  /* Check to see if the session manager restored us */
  restored = (gnome_client_get_flags (client) & GNOME_CLIENT_RESTORED) != 0;

  if (!restored) {
    /* We want to display the dialog box, but we do want to check if the notification
     * icon should be added
     */
     gtk_widget_show_all (GTK_WIDGET (battfink_client_window_new (fink_client)));
  }

  battfink_app_server = battfink_application_server_new ();
  gtk_main ();
}
