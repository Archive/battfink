#ifndef __BATTFINK_DPMS_H__
#define __BATTFINK_DPMS_H__

#include "config.h"
#include <glib.h>

void
battfink_get_dpms_timeouts (gint *standby, gint *suspend, gint *shutdown, GError **error);

void
battfink_set_dpms_timeouts (gint standby, gint suspend, gint shutdown, GError **error);

gboolean
battfink_check_dpms_enabled (GError **error);

void
battfink_dpms_enable (gboolean state, GError **error);

typedef enum {
  BATTFINK_DPMS_QUERY_ERROR,
  BATTFINK_DPMS_SUPPORT_ERROR,
  BATTFINK_DPMS_INFO_ERROR,
  BATTFINK_DPMS_SET_ERROR,
  BATTFINK_DPMS_GET_ERROR,
  BATTFINK_DPMS_ENABLE_ERROR,
  BATTFINK_DPMS_DISABLE_ERROR,
} BattfinkDPMSError;

#define BATTFINK_DPMS_ERROR (battfink_dpms_error_quark ())
GQuark battfink_dpms_error_quark (void) G_GNUC_CONST;

#endif /* __BATTFINK_DPMS_H__ */
