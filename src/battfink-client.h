#ifndef __BATTFINK_CLIENT_H__
#define __BATTFINK_CLIENT_H__

#include "config.h"
#include <gtk/gtk.h>

#define BATTFINK_TYPE_CLIENT                  (battfink_client_get_type ())
#define BATTFINK_CLIENT(obj)                  (GTK_CHECK_CAST ((obj), BATTFINK_TYPE_CLIENT, BattfinkClient))
#define BATTFINK_CLIENT_CLASS(klass)          (GTK_CHECK_CLASS_CAST ((klass), BATTFINK_TYPE_CLIENT, BattfinkClientClass))
#define BATTFINK_IS_CLIENT(obj)               (GTK_CHECK_TYPE ((obj), BATTFINK_TYPE_CLIENT))
#define BATTFINK_IS_CLIENT_CLASS(klass)       (GTK_CHECK_CLASS_TYPE ((klass), BATTFINK_TYPE_CLIENT))
#define BATTFINK_CLIENT_GET_CLASS(obj)        (GTK_CHECK_GET_CLASS ((obj), BATTFINK_TYPE_CLIENT, BattfinkClientClass))

typedef struct _BattfinkClient             BattfinkClient;
typedef struct _BattfinkClientClass        BattfinkClientClass;

typedef struct _BattfinkClientPriv         BattfinkClientPriv;

struct _BattfinkClient
{
          GObject parent;

          BattfinkClientPriv *priv;
};

struct _BattfinkClientClass
{
          GObjectClass parent_class;
};


GType           battfink_client_get_type      (void) G_GNUC_CONST;

BattfinkClient* battfink_client_new           (void);
GtkWindow *     battfink_client_window_new    (BattfinkClient *fink_client);
GtkWindow *     battfink_client_get_active_window (BattfinkClient *fink_client);
void            battfink_client_set_active_window (BattfinkClient *fink_client, GtkWindow *window);

#endif /* __BATTFINK_CLIENT_H__ */
