#ifndef __BATTFINK_APPLICATION_SERVER_H
#define __BATTFINK_APPLICATION_SERVER_H

#include "GNOME_Battfink.h"
#include <bonobo/bonobo-control.h>
#include <bonobo/bonobo-object.h>

G_BEGIN_DECLS

#define BATTFINK_APPLICATION_SERVER_TYPE         (battfink_application_server_get_type ())
#define BATTFINK_APPLICATION_SERVER(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), BATTFINK_APPLICATION_SERVER_TYPE, BattfinkApplicationServer))
#define BATTFINK_APPLICATION_SERVER_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), BATTFINK_APPLICATION_SERVER_TYPE, BattfinkApplicationServerClass))
#define BATTFINK_APPLICATION_SERVER_IS_OBJECT(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), BATTFINK_APPLICATION_SERVER_TYPE))
#define BATTFINK_APPLICATION_SERVER_IS_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), BATTFINK_APPLICATION_SERVER_TYPE))
#define BATTFINK_APPLICATION_SERVER_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), BATTFINK_APPLICATION_SERVER_TYPE, BattfinkApplicationServerClass))

typedef struct
{
        BonoboObject parent;
} BattfinkApplicationServer;

typedef struct
{
        BonoboObjectClass parent_class;

        POA_GNOME_Battfink_Application__epv epv;
} BattfinkApplicationServerClass;

GType          battfink_application_server_get_type (void);

BonoboObject  *battfink_application_server_new      (void);

G_END_DECLS

#endif /* __BATTFINK_APPLICATION_SERVER_H */
