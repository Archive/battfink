#ifndef __BATTFINK_ACPI_H__
#define __BATTFINK_ACPI_H__

gboolean    battfink_acpi_read_info (struct apm_info *apminfo);

#endif /* __BATTFINK_ACPI_H__ */
