/*
 * battfink-client.c
 *
 * Copyright (C) 2003 Sun Microsystems, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Glynn Foster <glynn.foster@sun.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glade/glade.h>
#include <gconf/gconf-client.h>

#include <libgnome/gnome-help.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomeui/gnome-window-icon.h>

#include "battfink-client.h"
#include "battfink-dpms.h"
#include "battfink-apm.h"
#include "eggtrayicon.h"

static void battfink_client_class_init        (BattfinkClientClass *klass);
static void battfink_client_init              (BattfinkClient *fink);
static void battfink_client_finalize          (GObject        *object);

static GObjectClass *parent_class = NULL;

#define BATTFINK_GLADE_FILE BATTFINK_DATADIR "/battfink.glade"
#define BATTFINK_GLADE_LOCAL_FILE "./battfink.glade"

#define BATTFINK_GCONF_KEY "/apps/battfink/"
#define BATTFINK_GCONF_DIR "/apps/battfink"

struct _BattfinkClientPriv {
        GtkWindow     *active_window;
        GSList        *windows;
        BattfinkApm   *fink_apm;
        EggTrayIcon   *tray_icon;

        gint           battery_warning_timeout;
};

GConfClient *
battfink_client_get_gconf_client ()
{
  static GConfClient *client;

  if (client == NULL) {
    GError *error = NULL;

    client = gconf_client_get_default ();
    gconf_client_add_dir (client, BATTFINK_GCONF_DIR,
                          GCONF_CLIENT_PRELOAD_NONE, &error);
    if (error) {
      g_warning ("Failed to monitor '%s' : %s", BATTFINK_GCONF_KEY, error->message);
      g_error_free (error);
    }

  }

  return client;
}

GType
battfink_client_get_type (void)
{
  static GType fink_type = 0;

  if (fink_type == 0) {
    static const GTypeInfo our_info = {
        sizeof (BattfinkClientClass),
        NULL,           /* base_init */
        NULL,           /* base_finalize */
        (GClassInitFunc) battfink_client_class_init,
        NULL,           /* class_finalize */
        NULL,           /* class_data */
        sizeof (BattfinkClient),
        0,              /* n_preallocs */
        (GInstanceInitFunc) battfink_client_init
    };

  fink_type = g_type_register_static (G_TYPE_OBJECT,
                                      "BattfinkClient",
                                      &our_info, 0);

  }
  return fink_type;
}

static void
battfink_client_class_init (BattfinkClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = battfink_client_finalize;
}

static void
battfink_battery_icon_changed_callback (GConfClient *client, guint id, GConfEntry *entry, BattfinkClient *fink)
{
  gboolean value;

  if (!entry->value || entry->value->type != GCONF_VALUE_BOOL)
    return;

  value = gconf_value_get_bool (entry->value);

  if (value)
    BATTFINK_APM_GET_CLASS (fink->priv->fink_apm)->enable (fink->priv->fink_apm, fink->priv->tray_icon);
  else
    BATTFINK_APM_GET_CLASS (fink->priv->fink_apm)->disable (fink->priv->fink_apm, fink->priv->tray_icon);
}

static void
battfink_battery_label_changed_callback (GConfClient *client, guint id, GConfEntry *entry, BattfinkClient *fink)
{
  gboolean value;

  if (!entry->value || entry->value->type != GCONF_VALUE_BOOL)
    return;

  value = gconf_value_get_bool (entry->value);

  if (value)
    BATTFINK_APM_GET_CLASS (fink->priv->fink_apm)->enable_label (fink->priv->fink_apm, fink->priv->tray_icon);
  else
    BATTFINK_APM_GET_CLASS (fink->priv->fink_apm)->disable_label (fink->priv->fink_apm, fink->priv->tray_icon);
}

static void battfink_client_init_dialog (GladeXML *glade_xml, BattfinkClient *fink_client);
static GladeXML * battfink_client_load_xml (void);

static void
battfink_client_invoke_preferences (BattfinkApm *fink_apm, gpointer user_data)
{
  GtkWindow *window;
  BattfinkClient *fink_client;
  GladeXML *glade_xml;
  GtkWidget *dialog;

  window = battfink_client_get_active_window (BATTFINK_CLIENT (fink_apm->parent));
  if (window) {
    gtk_window_present (window);
    return;
  }

  fink_client = BATTFINK_CLIENT (fink_apm->parent);
  glade_xml = battfink_client_load_xml ();
  dialog = glade_xml_get_widget (glade_xml, "battfink_dialog");
  battfink_client_init_dialog (glade_xml, fink_client);

  fink_client->priv->windows = g_slist_append (fink_client->priv->windows, GTK_WINDOW (dialog));
  battfink_client_set_active_window (fink_client, GTK_WINDOW (dialog));
}

static void
battfink_client_disable_icon (BattfinkApm *fink_apm)
{
  GConfClient *client;

  client = battfink_client_get_gconf_client ();

  gconf_client_set_bool (client,
                         BATTFINK_GCONF_KEY "show_battery_status",
                         FALSE, NULL);
}

static void
battfink_client_disable_label (BattfinkApm *fink_apm)
{
  GConfClient *client;

  client = battfink_client_get_gconf_client ();

  gconf_client_set_bool (client,
                         BATTFINK_GCONF_KEY "show_label",
                         FALSE, NULL);
}

static void
battfink_client_init (BattfinkClient *fink)
{
  GConfClient *client;
  gboolean battery_value = FALSE;
  gboolean label_value = FALSE;
  
  fink->priv = g_new0 (BattfinkClientPriv, 1);
  fink->priv->fink_apm = g_object_new (BATTFINK_TYPE_APM, NULL);

  /* FIXME : This is kind of evil, but I need a reference to the 
   * parent BattfinkClient
   */
  fink->priv->fink_apm->parent = fink;

  fink->priv->tray_icon = egg_tray_icon_new ("Battfink");
  g_signal_connect (G_OBJECT (fink->priv->fink_apm), "preferences",
                    G_CALLBACK (battfink_client_invoke_preferences), fink);
  g_signal_connect (G_OBJECT (fink->priv->fink_apm), "disable",
                    G_CALLBACK (battfink_client_disable_icon), NULL);
  g_signal_connect (G_OBJECT (fink->priv->fink_apm), "disable_label",
                    G_CALLBACK (battfink_client_disable_label), NULL);

  gtk_container_add (GTK_CONTAINER (fink->priv->tray_icon), GTK_WIDGET (fink->priv->fink_apm));

  client = battfink_client_get_gconf_client ();
#ifdef HAVE_APM
  battery_value = gconf_client_get_bool (client,
                                         BATTFINK_GCONF_KEY "show_battery_status", NULL);
  gconf_client_notify_add (client,
                           BATTFINK_GCONF_KEY "show_battery_status",
                           (GConfClientNotifyFunc) battfink_battery_icon_changed_callback,
                           fink, NULL, NULL);
  if (battery_value)
    BATTFINK_APM_GET_CLASS (fink->priv->fink_apm)->enable (fink->priv->fink_apm, fink->priv->tray_icon);
  
  label_value = gconf_client_get_bool (client,
                                       BATTFINK_GCONF_KEY "show_label", NULL);
  gconf_client_notify_add (client,
                           BATTFINK_GCONF_KEY "show_label",
                           (GConfClientNotifyFunc) battfink_battery_label_changed_callback,
                           fink, NULL, NULL);
  if (label_value)
    BATTFINK_APM_GET_CLASS (fink->priv->fink_apm)->enable_label (fink->priv->fink_apm, fink->priv->tray_icon);
#endif
}

static void
battfink_client_finalize (GObject *object)
{
  BattfinkClient *fink;

  g_return_if_fail (object != NULL);

  fink = BATTFINK_CLIENT (object);

  g_source_remove (fink->priv->battery_warning_timeout);
  g_return_if_fail (BATTFINK_IS_CLIENT (fink));
  g_return_if_fail (fink->priv != NULL);


  G_OBJECT_CLASS (parent_class)->finalize (object);
}

BattfinkClient*
battfink_client_new (void)
{
  BattfinkClient *fink;

  fink = BATTFINK_CLIENT (g_object_new (BATTFINK_TYPE_CLIENT, NULL));
  g_return_val_if_fail (fink != NULL, NULL);

  return fink;
}

static GladeXML *
battfink_client_load_xml (void)
{
  GladeXML *xml = NULL;

  if (g_file_test (BATTFINK_GLADE_LOCAL_FILE, G_FILE_TEST_EXISTS)) {
    /* Try current dir, for debugging */
    xml = glade_xml_new (BATTFINK_GLADE_LOCAL_FILE, "battfink_dialog", GETTEXT_PACKAGE);
  }

  if (xml == NULL)
    xml = glade_xml_new (BATTFINK_GLADE_FILE, "battfink_dialog", GETTEXT_PACKAGE);

  if (xml == NULL) {
    g_warning ("Could not load glade file : %s", BATTFINK_GLADE_FILE);
    return NULL;
  }
  
  return xml;
}

static void
battfink_dialog_response_callback (GtkDialog *dialog, gint button, gpointer user_data)
{
  GError *error = NULL;

  switch (button) {
    case GTK_RESPONSE_CLOSE:
      gtk_widget_hide (GTK_WIDGET (dialog));
      break;
    case GTK_RESPONSE_HELP:
      gnome_help_display ("battfink", "battfink", &error);
      if (error) {
        GtkWidget *message_dialog;

        message_dialog = gtk_message_dialog_new (GTK_WINDOW (dialog),
                                                 GTK_DIALOG_DESTROY_WITH_PARENT,
                                                 GTK_MESSAGE_ERROR,
                                                 GTK_BUTTONS_CLOSE,
                                                 _("There was an error displaying help:\n%s"), 
                                                 error->message);
        g_error_free (error);
        g_signal_connect (G_OBJECT (message_dialog), "response",
                          G_CALLBACK (gtk_widget_destroy), NULL);
        gtk_window_set_resizable (GTK_WINDOW (message_dialog), FALSE);
        gtk_widget_show (message_dialog);
      }
      break;
    default:
      break;
  }
}

static void
battfink_dialog_destroy_callback (GtkDialog *dialog, gpointer user_data)
{
  gtk_widget_hide (GTK_WIDGET (dialog));
}

static void
battfink_battery_toggle_callback (GtkToggleButton *toggle, gpointer user_data)
{
  GConfClient *client;
  GladeXML *glade_xml = GLADE_XML (user_data);
  gboolean state;
  GtkWidget *label_toggle = glade_xml_get_widget (glade_xml, "battfink_label_checkbox");

  client = battfink_client_get_gconf_client ();

  state = gtk_toggle_button_get_active (toggle);
  gtk_widget_set_sensitive (label_toggle, state);

  gconf_client_set_bool (client,
                         BATTFINK_GCONF_KEY "show_battery_status",
                         state, NULL);
}

static void
battfink_battery_label_toggle_callback (GtkToggleButton *toggle, gpointer user_data)
{
  GConfClient *client;
  gboolean state;

  client = battfink_client_get_gconf_client ();

  state = gtk_toggle_button_get_active (toggle);

  gconf_client_set_bool (client,
                         BATTFINK_GCONF_KEY "show_label",
                         state, NULL);
}

static void
battfink_display_toggle_callback (GtkToggleButton *toggle, gpointer user_data)
{
  GladeXML *glade_xml = GLADE_XML (user_data);
  GtkWidget *display_table;
  GError *error = NULL;
  gboolean state;

  state = gtk_toggle_button_get_active (toggle);
  display_table = glade_xml_get_widget (glade_xml, "battfink_display_table");
  gtk_widget_set_sensitive (display_table, state);

  battfink_dpms_enable (state, &error);
  if (error) {
    g_warning ("Failed to enable DPMS : %s", error->message);
    g_error_free (error);
  }
}

static void
battfink_battery_changed_callback (GConfClient *client, guint id, GConfEntry *entry, GtkToggleButton *toggle)
{
  gboolean value;

  if (!entry->value || entry->value->type != GCONF_VALUE_BOOL)
    return;

  value = gconf_value_get_bool (entry->value);

  /* Just simply toggle the checkbutton here, since we have other notifications
   * on the same key to enable/disable the battery notification icon
   */
  if (gtk_toggle_button_get_active (toggle) != value)
    gtk_toggle_button_set_active (toggle, value);
}

static gboolean
battfink_client_check_dpms (gpointer user_data)
{
  GladeXML *glade_xml = GLADE_XML (user_data);
  GtkWidget *toggle;
  GtkWidget *table;

  GtkWidget *standby_spin;
  GtkWidget *suspend_spin;
  GtkWidget *shutdown_spin;

  GError *error = NULL;

  gint standby, suspend, shutdown;
  static gint standby_old, suspend_old, shutdown_old;

  gboolean power_state;
  gboolean value;

  power_state = battfink_check_dpms_enabled (&error);

  if (error) {
    g_warning ("Failed to check DPMS : %s", error->message);
    g_error_free (error);
    return TRUE;
  }
  error = NULL;

  battfink_get_dpms_timeouts (&standby, &suspend, &shutdown, &error);

  if (error) {
    g_warning ("Failed to get DPMS timeouts: %s", error->message);
    g_error_free (error);
    return TRUE;
  }

  if (standby == standby_old && suspend == suspend_old && shutdown == shutdown_old)
    return TRUE;
 
  standby_old = standby;
  suspend_old = suspend;
  shutdown_old = shutdown;

  toggle = glade_xml_get_widget (glade_xml, "battfink_display_checkbox");
  table = glade_xml_get_widget (glade_xml, "battfink_display_table");

  standby_spin = glade_xml_get_widget (glade_xml, "battfink_display_standby_spinbutton");
  suspend_spin = glade_xml_get_widget (glade_xml, "battfink_display_suspend_spinbutton");
  shutdown_spin = glade_xml_get_widget (glade_xml, "battfink_display_shutdown_spinbutton");
  
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (toggle)) != power_state) {
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (toggle), power_state);
    gtk_widget_set_sensitive (table, power_state);
  }

  /* Let's fill in the values as minutes */
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (standby_spin), (gdouble) standby / 60.0);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (suspend_spin), (gdouble) suspend / 60.0);
  gtk_spin_button_set_value (GTK_SPIN_BUTTON (shutdown_spin), (gdouble) shutdown / 60.0);

  return TRUE;
}

static void
battfink_display_spin_callback (GtkSpinButton *spin, gpointer user_data)
{
  GladeXML *glade_xml = user_data;
  static GtkWidget *suspend_spin;
  static GtkWidget *standby_spin;
  static GtkWidget *shutdown_spin;
  GError *error = NULL;
  gint suspend, standby, shutdown;
  gdouble min, max;

  standby_spin = glade_xml_get_widget (glade_xml, "battfink_display_standby_spinbutton");
  suspend_spin = glade_xml_get_widget (glade_xml, "battfink_display_suspend_spinbutton");
  shutdown_spin = glade_xml_get_widget (glade_xml, "battfink_display_shutdown_spinbutton");

  standby = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (standby_spin));
  suspend = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (suspend_spin));
  shutdown = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (shutdown_spin));

  gtk_spin_button_set_range (GTK_SPIN_BUTTON (standby_spin), 5.0, (gdouble) suspend);
  gtk_spin_button_set_range (GTK_SPIN_BUTTON (suspend_spin), (gdouble) standby, (gdouble) shutdown);
  gtk_spin_button_set_range (GTK_SPIN_BUTTON (shutdown_spin), (gdouble) suspend, 300.0);
  
  battfink_set_dpms_timeouts (standby * 60, suspend * 60, shutdown * 60, NULL);

  if (error) {
    g_warning ("Failed to set DPMS timeouts : %s", error->message);
    g_error_free (error);
  }
}

static void
battfink_client_warning_response (GtkWidget *widget, gint response, gpointer user_data)
{
  switch (response) {
    default:
      gtk_widget_destroy (widget);
  }
}

static gboolean
battfink_client_check_battery (gpointer user_data)
{
  BattfinkClient *fink;
  GtkWidget *widget;
  static gboolean warning = FALSE;
  gint time_left;
 
  fink = BATTFINK_CLIENT (user_data);
  time_left = BATTFINK_APM_GET_CLASS (fink->priv->fink_apm)->time_left (fink->priv->fink_apm);

  if (time_left > 0) {
    if (!warning && time_left < 10) {
      /* We warn the user that there is not much time left on the battery */
      widget = gtk_message_dialog_new (fink->priv->active_window,
                                       GTK_DIALOG_DESTROY_WITH_PARENT,
                                       GTK_MESSAGE_WARNING,
                                       GTK_BUTTONS_OK,
                                       _("Your battery is critically low.\n"
                                         "You should switch to AC power immediately to avoid losing your work"));
      g_signal_connect (widget, "response",
                        G_CALLBACK (battfink_client_warning_response), NULL);
      gtk_widget_show (widget);
      warning = TRUE;
    }
  } else {
    warning = FALSE;
  }

  return TRUE;
}


static void
battfink_client_init_dialog (GladeXML *glade_xml, BattfinkClient *fink_client)
{
  GConfClient *client;

  GtkWidget *dialog;
  GtkWidget *display_toggle;
  GtkWidget *battery_vbox;
  GtkWidget *battery_toggle;
  GtkWidget *label_toggle;
  GtkWidget *display_table;
  GtkWidget *standby_spin;
  GtkWidget *suspend_spin;
  GtkWidget *shutdown_spin;

  gboolean display_value, battery_value = FALSE, label_value = FALSE;
  gint standby, suspend, off;

  client = battfink_client_get_gconf_client ();

  dialog = glade_xml_get_widget (glade_xml, "battfink_dialog");

  display_toggle = glade_xml_get_widget (glade_xml, "battfink_display_checkbox");
  battery_toggle = glade_xml_get_widget (glade_xml, "battfink_battery_checkbox");
  label_toggle = glade_xml_get_widget (glade_xml, "battfink_label_checkbox");
  battery_vbox = glade_xml_get_widget (glade_xml, "battfink_battery_vbox");

  display_table = glade_xml_get_widget (glade_xml, "battfink_display_table");

  standby_spin = glade_xml_get_widget (glade_xml, "battfink_display_standby_spinbutton");
  suspend_spin = glade_xml_get_widget (glade_xml, "battfink_display_suspend_spinbutton");
  shutdown_spin = glade_xml_get_widget (glade_xml, "battfink_display_shutdown_spinbutton");

  /* If the value that we get from DPMS is different, then the user must have 
   * configured it seperately from the dialog. We have no way of getting any
   * notification for this, so let's just try and keep in sync as best we can.
   */
  battfink_client_check_dpms (glade_xml);

  g_timeout_add (3000, battfink_client_check_dpms, glade_xml);

#ifdef HAVE_APM
  battery_value = gconf_client_get_bool (client,
                                         BATTFINK_GCONF_KEY "show_battery_status", NULL);
  label_value = gconf_client_get_bool (client,
                                       BATTFINK_GCONF_KEY "show_label", NULL);
  
  if (battery_value) {
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (battery_toggle), TRUE);
    gtk_widget_set_sensitive (label_toggle, TRUE);
  } else {
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (battery_toggle), FALSE);
    gtk_widget_set_sensitive (label_toggle, FALSE);
  }

  if (label_value)
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (label_toggle), TRUE);
  else
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (label_toggle), FALSE);

  gconf_client_notify_add (client,
                           BATTFINK_GCONF_KEY "show_battery_status",
                           (GConfClientNotifyFunc) battfink_battery_changed_callback,
                           battery_toggle, NULL, NULL);

  g_signal_connect (G_OBJECT (battery_toggle), "toggled",
                    G_CALLBACK (battfink_battery_toggle_callback), glade_xml);
  
  gconf_client_notify_add (client,
                           BATTFINK_GCONF_KEY "show_label",
                           (GConfClientNotifyFunc) battfink_battery_changed_callback,
                           label_toggle, NULL, NULL);

  g_signal_connect (G_OBJECT (label_toggle), "toggled",
                    G_CALLBACK (battfink_battery_label_toggle_callback), NULL);

  fink_client->priv->battery_warning_timeout = g_timeout_add (5000, battfink_client_check_battery, fink_client);
#else
    /* Hide the battery checkbox stuff */
  gtk_widget_hide (battery_vbox);
#endif

  g_signal_connect (G_OBJECT (dialog), "response",
                    G_CALLBACK (battfink_dialog_response_callback), glade_xml);
  
  g_signal_connect (G_OBJECT (dialog), "delete_event",
                    G_CALLBACK (battfink_dialog_destroy_callback), glade_xml);

  g_signal_connect (G_OBJECT (display_toggle), "toggled",
                    G_CALLBACK (battfink_display_toggle_callback), glade_xml);

  g_signal_connect (G_OBJECT (standby_spin), "value-changed",
                    G_CALLBACK (battfink_display_spin_callback), glade_xml);
  g_signal_connect (G_OBJECT (suspend_spin), "value-changed",
                    G_CALLBACK (battfink_display_spin_callback), glade_xml);
  g_signal_connect (G_OBJECT (shutdown_spin), "value-changed",
                    G_CALLBACK (battfink_display_spin_callback), glade_xml);
}

GtkWindow *
battfink_client_window_new (BattfinkClient *fink_client)
{
  GtkWidget *dialog;
  GladeXML *glade_xml;

  glade_xml = battfink_client_load_xml (); 

  if (glade_xml == NULL) {
    return NULL;
  }

  glade_xml_signal_autoconnect (glade_xml);
  dialog = glade_xml_get_widget (glade_xml, "battfink_dialog");
  gnome_window_icon_set_from_file (GTK_WINDOW (dialog), GNOME_ICONDIR "/battfink.png");
  battfink_client_init_dialog (glade_xml, fink_client);

  fink_client->priv->windows = g_slist_append (fink_client->priv->windows, GTK_WINDOW (dialog));

  battfink_client_set_active_window (fink_client, GTK_WINDOW (dialog));
  return GTK_WINDOW (dialog);
}

void
battfink_client_set_active_window (BattfinkClient *fink_client, GtkWindow *window)
{
  fink_client->priv->active_window = window;
}

GtkWindow *
battfink_client_get_active_window (BattfinkClient *fink_client) 
{
  return fink_client->priv->active_window;
}
