/*
 * battfink-app-server.c
 *
 * Copyright (C) 2003 Sun Microsystems, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Glynn Foster <glynn.foster@sun.com>
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <bonobo/bonobo-generic-factory.h>
#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-context.h>
#include <bonobo-activation/bonobo-activation-register.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <gconf/gconf-client.h>

#include "GNOME_Battfink.h"
#include "battfink-app-server.h"
#include "battfink-win-server.h"

#include "header.h"

static void battfink_application_server_class_init      (BattfinkApplicationServerClass *klass);
static void battfink_application_server_init            (BattfinkApplicationServer *a);
static void battfink_application_server_object_finalize (GObject *object);

static GObjectClass *battfink_application_server_parent_class;

static BonoboObject *
battfink_application_server_factory (BonoboGenericFactory *this_factory,
			             const char *iid,
			             gpointer user_data)
{
  BattfinkApplicationServer *a;
        
  a  = g_object_new (BATTFINK_APPLICATION_SERVER_TYPE, NULL);

  return BONOBO_OBJECT (a);
}

BonoboObject *
battfink_application_server_new (void)
{
  BonoboGenericFactory *factory;

  factory = bonobo_generic_factory_new ("OAFIID:GNOME_Battfink_Factory",
                                        battfink_application_server_factory,
                                        NULL);
  return BONOBO_OBJECT (factory);
}

static GNOME_Battfink_Window
impl_battfink_application_server_newWindow (PortableServer_Servant _servant,
					    CORBA_Environment * ev)
{
  GtkWindow *win;
  BonoboObject *win_server;

  /* let the UI update */
  while (gtk_events_pending ())
    gtk_main_iteration ();

  battfink_client_window_new (fink_client);
  win = battfink_client_get_active_window (fink_client);

  win_server = battfink_window_server_new (win);

  return BONOBO_OBJREF (win_server);
}


static GNOME_Battfink_Window
impl_battfink_application_server_getActiveWindow (PortableServer_Servant _servant,
					          CORBA_Environment * ev)
{
  GtkWindow *win;
  BonoboObject *win_server;

  win = battfink_client_get_active_window (fink_client);

  win_server = battfink_window_server_new (win);
  return BONOBO_OBJREF (win_server);
}

static void
impl_battfink_application_server_quit (PortableServer_Servant _servant,
                                       CORBA_Environment * ev)
{
  gtk_main_quit ();
}

static void
battfink_application_server_class_init (BattfinkApplicationServerClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  POA_GNOME_Battfink_Application__epv *epv = &klass->epv;

  battfink_application_server_parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = battfink_application_server_object_finalize;

  /* connect implementation callbacks */
  epv->newWindow         = impl_battfink_application_server_newWindow;
  epv->getActiveWindow   = impl_battfink_application_server_getActiveWindow;

  epv->quit              = impl_battfink_application_server_quit;
}

static void
battfink_application_server_init (BattfinkApplicationServer *c) 
{
}

static void
battfink_application_server_object_finalize (GObject *object)
{
  BattfinkApplicationServer *a = BATTFINK_APPLICATION_SERVER (object);

  battfink_application_server_parent_class->finalize (G_OBJECT (a));
}

BONOBO_TYPE_FUNC_FULL (
        BattfinkApplicationServer,                    
        GNOME_Battfink_Application, 
        BONOBO_TYPE_OBJECT,           
        battfink_application_server);
